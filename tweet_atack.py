import tweepy
from config import CONFIG
import time 

CONSUMER_KEY = CONFIG["CONSUMER_KEY"]
CONSUMER_SECRET = CONFIG["CONSUMER_SECRET"]
ACCESS_TOKEN = CONFIG["ACCESS_TOKEN"]
ACCESS_SECRET = CONFIG["ACCESS_SECRET"]


auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
api = tweepy.API(auth)

search_results = api.search(q="#wj22", count=100)

for result in search_results:
    time.sleep(1)
    print(result)
    print()
    print(result.text)
    tweet_id = result.id
    user_id = result.user._json['id']
    try:
        api.create_friendship(user_id) # ←追記
    except Exception as e:
        print(e)
