from sudachipy import tokenizer
from sudachipy import dictionary
import db_util
import util
import time

class SudachiTokenizer():

    def __init__(self):
        self.tokenizer_obj = dictionary.Dictionary().create()
        self.mode = tokenizer.Tokenizer.SplitMode.C

    def tokenize(self, sentence):
        list = []
        for word in self.tokenizer_obj.tokenize(sentence, self.mode):
            list.append(word.dictionary_form())
        return list
    def selection(self, sentence):
        list = []
        for word in sentence:
            word=self.tokenizer_obj.tokenize(word, self.mode)[0]
            if word.part_of_speech()[0] in ["動詞","名詞","形容詞","形状詞","副詞","代名詞","連体詞","感動詞"]:
                list.append(word.dictionary_form())
        return list

    def sudachi_category(self,category_id):
        if category_id==0:
            rows=db_util.select_user_by_category_id(category_id,100,3600)
        else:
            rows=db_util.select_user_by_category_id(category_id,90,200)
        
        w_dict={}
        for row in rows:
            user_id=row[4]
            rows2=db_util.select_user_detail_for_sudachi(user_id)

            for row2 in rows2:
                text=row2[6]
                text=util.replace_linebreak(text)
                text=self.tokenize(text)
                text=self.selection(text)
                for word in text:
                    if word not in w_dict.keys():
                        w_dict[word] = 0
                    w_dict[word] +=1      
        
        w_dict=sorted(w_dict.items(), key=lambda x:x[1] ,reverse=True)
        return(w_dict)
    
    def sudachi_text(self,rows,w_dict):
        #テキスト取得
        #w_dict={}
        #rows=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,target_code)
        for row in rows:
            text=row[6]
            text=util.replace_linebreak(text)
            text=self.tokenize(text)
            text=self.selection(text)
            for word in text:
                if word not in w_dict.keys():
                    w_dict[word] = 0
                w_dict[word] +=1
        return(w_dict)
            
