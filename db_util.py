
#common==============================================================--
SEC=None
CONN=None
def initial_setup(sec):
    global SEC
    SEC=sec
    return(True)

def db_connect():
    import MySQLdb
    import time

    # 接続する 
    conn = MySQLdb.connect(
    unix_socket = '/Applications/MAMP/tmp/mysql/mysql.sock',
    user='root',
    passwd='ezakurah',
    host='localhost',
    db='tweetauth',
    use_unicode=True,
    charset="utf8mb4"
    )
    global CONN
    CONN=conn
    return(True)

def sql_exe(sql,fanc):
    conn=CONN
    rows = None
    try:
        # カーソルを取得する
        cur = conn.cursor()
        # SQL（データベースを操作するコマンド）を実行する
        # userテーブルから、HostとUser列を取り出す
        cur.execute(sql)
        # 実行結果を取得する
        rows = cur.fetchall()
        # 一行ずつ表示する
        #print(row) for row in rows
        
        cur.close()
        conn.commit()
    
    except Exception as e:
        #print(e)
        return(False)
    return(rows)

def select_util(id):
    sql="SELECT * FROM tweetauth.util WHERE ind = "+str(id)+" ;"
    rows=sql_exe(sql,"select_util")
    if len(rows) == 0:
        return(False)
    row=rows[0]
    return(row)

def update_util(id,user_id):
    sql="UPDATE tweetauth.util SET param="+str(user_id)+" WHERE ind = "+str(id)+" ;"
    rows=sql_exe(sql,"update_util")
    return(rows)

#user==============================================================--
def select_user(id):
    sql="SELECT * FROM tweetauth.user WHERE id = "+str(id)+" ;"
    rows=sql_exe(sql,"select_user")
    if len(rows) == 0:
        return(False)

    row=rows[0]
    return(row)

def select_users():
    sql="SELECT * FROM tweetauth.user ORDER BY core_date DESC LIMIT 100;"
    rows=sql_exe(sql,"select_users")
    return(rows)


def select_user_by_category_id(category_id,core_point,limit):
    sql="SELECT * FROM tweetauth.user WHERE category_id = "+str(category_id)+" AND core_point < "+str(core_point)+" ORDER BY core_date DESC LIMIT "+str(limit)+";"
    rows=sql_exe(sql,"select_user")
    return(rows)

def create_user(name,id,self_info):
    sql = "INSERT INTO user (`id`,`user_name`,`self`) VALUES ("+str(id)+",\""+name+"\",\""+str(self_info)+"\");"
    ans=sql_exe(sql,"create_user")
    return(ans)
    #プレポイント評価

def update_user(name,id,self_info,follow,follower):
    sql = "UPDATE tweetauth.user SET user_name=\""+name+"\", follow="+str(follow)+", follower="+str(follower)+", self=\""+str(self_info)+"\" WHERE id="+str(id)+";"
    ans=sql_exe(sql,"update_user")
    return(ans)

def update_user2(name,id,self_info,follow,follower,tweets_num,fv_num,rt_num,create_date,latest_date):
    sql = "UPDATE tweetauth.user SET follow="+str(follow)+", follower="+str(follower)+", self=\""+str(self_info)+"\" , tweets_num="+str(tweets_num)+", fv_num="+str(fv_num)+", rt_num="+str(rt_num)+", created_date="+str(create_date)+", latest_date="+str(latest_date)+" WHERE id="+str(id)+";"
    ans=sql_exe(sql,"update_user2")
    return(ans)

def update_user3(name,id,self_info,follow,follower,tweets_num,fv_num,create_date):
    sql = "UPDATE tweetauth.user SET user_name=\""+name+"\", follow="+str(follow)+", follower="+str(follower)+", self=\""+str(self_info)+"\" , tweets_num="+str(tweets_num)+", fv_num="+str(fv_num)+", created_date="+str(create_date)+" WHERE id="+str(id)+";"
    ans=sql_exe(sql,"update_user3")
    return(ans)

def update_core_point_3(id,utime,point,category_id):
    sql="UPDATE tweetauth.user SET core_point = "+str(point)+" ,core_date = "+str(utime)+" ,category_id = "+str(category_id)+" WHERE id = "+str(id)
    row=sql_exe(sql,"update_core_point_3")
    return(row)
def update_user_pre_point2(id,utime,point,pre_category_id):
    import util
    sql="UPDATE tweetauth.user SET pre_point = "+str(point)+" , pre_date = "+str(utime)+" , pre_category_id = "+str(pre_category_id)+" WHERE id = "+str(id)
    row=sql_exe(sql,"update_user_pre_point2")
    return(row)

def update_user_status(id,status):
    import util
    sql="UPDATE tweetauth.user SET status = "+str(status)+" WHERE id = "+str(id)
    row=sql_exe(sql,"update_user_status")
    return(row)

#user_point==============================================================--

def select_all_user_point(user_id):
    sql="SELECT * FROM tweetauth.user_point WHERE user_id = "+str(user_id)+" ;"
    rows=sql_exe(sql,"select_all_user_point")
    return(rows)

def select_user_point_by_category(user_id,point_id):
    sql="SELECT * FROM tweetauth.user_point WHERE user_id = "+str(user_id)+" and point_id = "+str(point_id)+";"
    rows=sql_exe(sql,"select_user_point_by_category")
    return(rows)

def select_old_user_self_point2(utime):
    sql="SELECT * FROM tweetauth.user_point ORDER BY self_date ASC LIMIT 1;"
    rows=sql_exe(sql,"select_old_user_self_point2")
    row=False
    if(len(rows)!=0):
        row=rows[0]
    return(row,SEC)

def select_low_point_user_from_user_point():
    import time
    utime = int(time.time())-SEC
    sql="SELECT * FROM tweetauth.user_point where self_point >= 0 and ( relation_date < "+str(utime)+" OR relation_date IS NULL ) ORDER by self_point ASC LIMIT 1;"
    rows=sql_exe(sql,"select_low_point_user_from_user_point")
    row=False
    if(len(rows)!=0):
        row=rows[0]
    return(row)

def select_low_point_user_from_user_point_by_category(point_id):
    import time
    utime = int(time.time())-SEC
    sql="SELECT * FROM tweetauth.user_point where self_point >= 0 AND point_id = "+str(point_id)+" AND ( relation_date < "+str(utime)+" OR relation_date IS NULL ) ORDER by self_point ASC LIMIT 1;"
    rows=sql_exe(sql,"select_low_point_user_from_user_point_by_category")
    row=False
    if(len(rows)!=0):
        row=rows[0]
    return(row)

def create_user_point(user_id,point_id):
    sql = "INSERT INTO user_point (`user_id`,`point_id`) VALUES ("+str(user_id)+","+str(point_id)+");"
    sql_exe(sql,"create_user_point")

def update_relation_point(id,utime,point,point_id):
    sql="UPDATE tweetauth.user_point SET relation_point = "+str(point)+" ,relation_date = "+str(utime)+" WHERE user_id = "+str(id)+" AND point_id = "+str(point_id)+";"
    sql_exe(sql,"update_relation_point")

def update_self_point(user_id,point_id,utime,point):
    sql="UPDATE tweetauth.user_point SET self_point = "+str(point)+" , self_date = "+str(utime)+" WHERE user_id = "+str(user_id)+" AND point_id = "+str(point_id)+";"
    sql_exe(sql,"update_self_point")

def update_relation_point_date(user_id,utime):
    sql="UPDATE tweetauth.user_point SET relation_date = "+str(utime)+" WHERE user_id = "+str(user_id)+";"
    rows=sql_exe(sql,"update_relation_point_date")
    return(rows)

def update_user_point_self_date(user_id,utime):
    sql="UPDATE tweetauth.user_point SET self_date = "+str(utime)+" WHERE user_id = "+str(user_id)+";"
    rows=sql_exe(sql,"update_user_point_self_date")
    return(rows)

#user_detail==============================================================--
def select_user_detail_by_userid(user_id):
    sql="SELECT * FROM tweetauth.user_detail where user_id = "+str(user_id)+";"
    rows=sql_exe(sql,"select_user_detail_by_userid_actcode_targetcode")
    return(rows)

def select_user_detail_by_userid_actcode_targetcode(user_id,act_code,target_code):
    sql="SELECT * FROM tweetauth.user_detail where user_id = "+str(user_id)+" AND act_code = "+str(act_code)+" AND target_code = "+str(target_code)+";"
    rows=sql_exe(sql,"select_user_detail_by_userid_actcode_targetcode")
    return(rows)

def select_user_detail_by_date(user_id,act_code,target_code):
    sql="SELECT * FROM tweetauth.user_detail where user_id = "+str(user_id)+" AND act_code = "+str(act_code)+" AND target_code = "+str(target_code)+" ORDER BY create_date ASC LIMIT 1;"
    rows=sql_exe(sql,"select_user_detail_by_date")
    if(len(rows)!=0):
        row=rows[0]
    return(row)

def select_user_detail_for_sudachi(user_id):
    sql="SELECT * from user_detail where TEXT IS NOT NULL AND TEXT !=\"\" AND user_id = "+str(user_id)+";"
    rows=sql_exe(sql,"def select_user_detail_for_sudachi")
    return(rows)

def create_user_detail(user_id,act_code,target_code):
    sql = "INSERT INTO tweetauth.user_detail (`user_id`,`create_date`,`act_code`,`target_code`) VALUES ("+str(user_id)+",1,"+str(act_code)+","+str(target_code)+");"
    sql_exe(sql,"create_user_detail")

def update_user_detail(id,relation_id,utime,text):
    import util
    sql="UPDATE tweetauth.user_detail SET relation_id = "+str(relation_id)+" , create_date = "+str(utime)+" , text = \""+str(text)+"\" WHERE id = "+str(id)+" ;"
    sql_exe(sql,"update_user_detail")

#table:relation_point------------------------------------------------------

def select_user_relation_point(user_id):
    sql="SELECT * FROM tweetauth.relation_point where user_id = "+str(user_id)+";"
    rows=sql_exe(sql,"select_user_relation_point")
    return(rows)

def select_user_relation_point_sum_by_category(user_id):
    #sql="SELECT user_id ,category_id,AVG(POINT) FROM tweetauth.relation_point where user_id = "+str(user_id)+" GROUP BY category_id having COUNT(*) > 5 ORDER BY category_id;"
    sql="SELECT user_id ,category_id, AVG(point) ,COUNT(*) AS count,SUM(POINT) FROM tweetauth.relation_point where user_id = "+str(user_id)+" and POINT != -1 GROUP BY category_id having COUNT(*) > 5 ORDER BY COUNT DESC;"
    rows=sql_exe(sql,"select_user_relation_point_sum_by_category")
    return(rows)

def select_user_relation_point_sum_by_category2(user_id,category_id):
    sql="SELECT *  FROM tweetauth.relation_point where user_id = "+str(user_id)+" and POINT != -1  AND category_id="+str(category_id)+" ORDER BY relation_point.point ASC;"
    rows=sql_exe(sql,"select_user_relation_point_sum_by_category2")
    return(rows)

def update_relation_point3(user_id,utime,act_code,target_code,category_id,point,index_num):
    import util
    #sql="UPDATE tweetauth.relation_point SET create_date = "+str(utime)+" , point = "+str(point)+" WHERE user_id = "+str(user_id)+" AND act_code = "+str(act_code)+" AND target_code = "+str(target_code)+" AND category_id = "+str(category_id)+" AND index_num = "+str(index_num)+" ;"
    sql="INSERT INTO tweetauth.relation_point (`user_id`,`create_date`,`act_code`,`target_code`,`category_id`,`index_num`,`point`) VALUES ("+str(user_id)+","+str(utime)+","+str(act_code)+","+str(target_code)+","+str(category_id)+","+str(index_num)+","+str(point)+") on duplicate key update create_date = "+str(utime)+" , point = "+str(point)+";"
    row=sql_exe(sql,"update_relation_point3")
    return(row)

#word_list==============================================================--
def update_word_list(rank,category_id,word,num,update_date,duplicate_flag):
    import util
    sql="INSERT INTO tweetauth.word_list (`rank`,`category_id`,`word`,`num`,`update_date`,`duplicate_flag`) VALUES ("+str(rank)+","+str(category_id)+",\""+word+"\","+str(num)+","+str(update_date)+","+str(duplicate_flag)+") on duplicate key update word=  \""+word+"\",num=  "+str(num)+",update_date=  "+str(update_date)+",duplicate_flag=  "+str(duplicate_flag)+";"
    row=sql_exe(sql,"update_word_list")
    return(row)

def update_word_list_duplicate(rank,category_id):
    import util
    sql="UPDATE tweetauth.word_list SET duplicate_flag=1 WHERE word_list.rank="+str(rank)+" AND word_list.category_id="+str(category_id)+";" 
    row=sql_exe(sql,"update_word_list_duplicate")
    return(row)
    
def select_word_list(rank,category_id):
    sql="SELECT * FROM tweetauth.word_list WHERE word_list.rank="+str(rank)+" AND word_list.category_id="+str(category_id)+";" 
    rows=sql_exe(sql,"select_word_list")
    row=rows[0]
    return(row)

def select_word_list_duplicate(category_id,word):
    sql="SELECT * FROM tweetauth.word_list WHERE word_list.category_id != "+str(category_id)+" AND word_list.word LIKE \"%"+str(word)+"%\" ;" 
    rows=sql_exe(sql,"select_word_list_duplicate")
    return(rows)

def select_word_list_duplicate2(category_id,word):
    sql="SELECT * FROM tweetauth.word_list WHERE word_list.category_id = 0 AND word_list.rank >= 100 AND word_list.word LIKE \"%"+str(word)+"%\" ;" 
    rows=sql_exe(sql,"select_word_list_duplicate")
    return(rows)
##########---user_words---------------------------------

def update_user_words(user_id,ranking,word,action_code,target_code,point,num,utime):
    sql="INSERT INTO tweetauth.user_words (`user_id`,`ranking`,`word`,`action_code`,`target_code`,`point`,`num`,`create_date`) VALUES ("+str(user_id)+","+str(ranking)+",\""+str(word)+"\","+str(action_code)+","+str(target_code)+","+str(point)+","+str(num)+","+str(utime)+") on duplicate key update create_date = "+str(utime)+" , word = \""+str(word)+"\" , point = "+str(point)+" , num = "+str(num)+";"
    row=sql_exe(sql,"update_user_word")
    return(row)

##########---user_subcategory---------------------------------
def select_user_subcategory_orderbydate(sub_category,limit):
    sql="SELECT * FROM tweetauth.user_subcategory where sub_category = "+str(sub_category)+" ORDER BY update_date ASC LIMIT "+str(limit)+";"
    rows=sql_exe(sql,"select_user_subcategory_orderbydate")
    return(rows)

def select_user_subcategory_forecast(category_id, forecast):
    sql="SELECT * FROM tweetauth.user_subcategory where category_id = "+str(category_id)+" and forecast_sub_category = "+str(forecast)+" LIMIT 100;"
    rows=sql_exe(sql,"select_user_subcategory_forecast")
    return(rows)

def update_user_subcategory(user_id, category_id, point, utime):
    sql="INSERT INTO tweetauth.user_subcategory (`user_id`, `category_id`, `point`,`sub_point`, `update_date`) VALUES ("+str(user_id)+","+str(category_id)+","+str(point)+","+str(point)+","+str(utime)+") on duplicate key update point = "+str(point)+" , category_id = "+str(category_id)+", sub_point = "+str(point)+";"
    row=sql_exe(sql,"update_user_subcategory")
    return(row)


def update_user_subpoint(user_id, category_id, point, sub_category, sub_point, utime):
    sql="INSERT INTO tweetauth.user_subcategory (`user_id`, `category_id`, `point`,`sub_category`, `sub_point`, `update_date`) VALUES ("+str(user_id)+","+str(category_id)+","+str(point)+","+str(sub_category)+","+str(sub_point)+","+str(utime)+") on duplicate key update point = "+str(point)+" , sub_point = "+str(sub_point)+" , category_id = "+str(category_id)+", update_date = "+str(utime)+";"
    row=sql_exe(sql,"update_user_subpoint")
    return(row)


##########---subcategory_word_list---------------------------------
def select_subcategory_word_list(category_id,act_code,target_code):
    sql="SELECT * FROM tweetauth.subcategory_word_list where category_id = "+str(category_id)+" and subcategory_id = 0 and act_code = "+str(act_code) +" and target_code = "+str(target_code) +";"
    rows=sql_exe(sql,"select_subcategory_word_list")
    return(rows)    

def select_subcategory_word_list_duplicate(category_id,subcategory_id,word,act_code,target_code):
    sql="SELECT * FROM tweetauth.subcategory_word_list WHERE category_id = "+str(category_id)+" and subcategory_id = "+str(subcategory_id) +"  and act_code = "+str(act_code) +" and target_code = "+str(target_code) + " AND ranking <= 100 AND word LIKE \"%"+str(word)+"%\" ;" 
    rows=sql_exe(sql,"select_subcategory_word_list_duplicate")
    return(rows)

def update_subcategory_word_list(category_id,subcategory_id,target_code,act_code,ranking,word,num):
    sql="INSERT INTO tweetauth.subcategory_word_list (`category_id`, `subcategory_id`, `target_code`, `act_code`, `ranking`, `word`, `num`) VALUES ("+str(category_id)+","+str(subcategory_id)+","+str(target_code)+","+str(act_code)+","+str(ranking)+",\""+word+"\","+str(num)+") on duplicate key update word=  \""+word+"\",num= "+str(num)+",duplicate_flag= "+str(0)+";"
    row=sql_exe(sql,"subcategory_word_list")
    return(row)

def update_subcategory_word_list_duplicate(category_id,act_code,target_code,ranking):
    import util
    sql="UPDATE tweetauth.subcategory_word_list SET duplicate_flag=1 WHERE ranking="+str(ranking)+" AND category_id="+str(category_id)+" and subcategory_id = 0 and act_code = "+str(act_code) +" and target_code = "+str(target_code) +";" 
    row=sql_exe(sql,"update_subcategory_word_list_duplicate")
    return(row)

def reset_subcategory_word_list_duplicate():
    import util
    sql="UPDATE tweetauth.subcategory_word_list SET duplicate_flag=0 where subcategory_id = 0;" 
    row=sql_exe(sql,"reset_subcategory_word_list_duplicate")
    return(row)





##########---follow_list---------------------------------

def update_follow_list(user_id,category_id,subcategory_id,follow_flag):
    sql="INSERT INTO `tweetauth`.`follow_list` (`user_id`, `category_id`, `subcategory_flag`, `follow_flag`) VALUES ("+str(user_id)+","+str(category_id)+","+str(subcategory_id)+","+str(follow_flag)+") on duplicate key update user_id="+str(user_id)+",follow_flag= "+str(follow_flag)+";"
    row=sql_exe(sql,"update_follow_list")
    return(row)






