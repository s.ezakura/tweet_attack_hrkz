from sudachipy import tokenizer
from sudachipy import dictionary
import db_util
import util
import time

class SudachiTokenizer():

    def __init__(self):
        self.tokenizer_obj = dictionary.Dictionary().create()
        self.mode = tokenizer.Tokenizer.SplitMode.C

    def tokenize(self, sentence):
        list = []
        for word in self.tokenizer_obj.tokenize(sentence, self.mode):
            list.append(word.dictionary_form())
            #list.append(word.surface())
        return list
    def selection(self, sentence):
        list = []
        for word in sentence:
            word=self.tokenizer_obj.tokenize(word, self.mode)[0]
            if word.part_of_speech()[0] in ["動詞","名詞","形容詞","形状詞","副詞","代名詞","連体詞","感動詞"]:
                list.append(word.dictionary_form())
        return list

    def sudachi_category(self,category_id):
        if category_id==0:
            rows=db_util.select_user_by_category_id(category_id,100,3600)
        else:
            rows=db_util.select_user_by_category_id(category_id,90,200)
        
        w_dict={}
        for row in rows:
            user_id=row[4]
            rows2=db_util.select_user_detail_for_sudachi(user_id)

            for row2 in rows2:
                text=row2[6]
                text=util.replace_linebreak(text)
                text=sudachi.tokenize(text)
                text=sudachi.selection(text)
                for word in text:
                    if word not in w_dict.keys():
                        w_dict[word] = 0
                    w_dict[word] +=1      
        
        w_dict=sorted(w_dict.items(), key=lambda x:x[1] ,reverse=True)
        return(w_dict)


util.initial_setup()
sudachi = SudachiTokenizer()

for i in range(1000):
    for category_id in range(0,17):
        w_dict=sudachi.sudachi_category(category_id)

        #1カテゴリ10マス確保
        if category_id==0:
            if len(w_dict) > 1600:
                w_dict=w_dict[:1600]
        else:
            if len(w_dict) > 1000:
                w_dict=w_dict[:1000]

        for r,text in enumerate(w_dict):
            db_util.update_word_list(r+1,category_id,text[0],text[1],int(time.time()),0)
        print("--------------"+str(category_id)+"--------------------")
        
        for category_id in range(1,17):
            for rank in range(1,100):
                row=db_util.select_word_list(rank,category_id)
                word=row[2]
                row=db_util.select_word_list_duplicate2(category_id,word)
                if len(row)!=0:
                    db_util.update_word_list_duplicate(rank,category_id)

        time.sleep(3600)

    time.sleep(7200)
