"""
#カテゴリ対応?
def update_core_points(user_id,dictionary,utime):
    import db_util
    point=0
    c=0
    for user_r_id,value in dictionary.items():
        rows=db_util.select_user_point(user_r_id,1)
        for row in rows:
            if(int(row[2])!=-1):
                point+=int(row[2])
                c+=1

        rows=db_util.select_user_point(user_r_id,2)
        for row in rows:
            if(int(row[2])!=-1):
                point+=int(row[2])
                c+=1
        
        rows=db_util.select_user_point(user_r_id,3)
        for row in rows:
            if(int(row[2])!=-1):
                point+=int(row[2])
                c+=1

    if(c!=0):
        point*=2
        point=point//c
    else:
        point=-1
    db_util.update_core_point_2(user_id,utime,point)
#カテゴリ対応?
def update_relation_points(user_id,dictionary,utime):
    update_relation_point(user_id,1,dictionary,utime)
    update_relation_point(user_id,2,dictionary,utime)
    update_relation_point(user_id,3,dictionary,utime)

def update_relation_point(user_id,point_id,dictionary,utime):
    import db_util
    point=0
    c=0
    for user_r_id,value in dictionary.items():
        rows=db_util.select_user_point(user_r_id,point_id)
        for row in rows:
            if(int(row[2])!=-1):
                point+=int(row[2])
                c+=1
    if(c!=0):
        point=(point*10)//c
    else:
        point=-1
    db_util.update_relation_point(user_id,utime,point,point_id)

def update_core_point2(user_id,utime):
    import db_util
    point=100
    c=0
    rows=db_util.select_all_user_point(user_id)
    for row in rows:
        if(int(row[4])!=-1):
            point=min(int(row[4]),point)
            c+=1
    if(c==0):
        point=-1
    db_util.update_core_point_2(user_id,utime,point)

def get_low_point_user_name():
    import db_util
    user_id=get_low_point_user_id()
    row=db_util.select_user(user_id)
    user_name=row[5]
    return(user_name)

def create_fv_user_his(user_id,dictionary,utime):
    import db_util
    for fv_id,value in dictionary.items():
        name=value[0]
        self_info=value[1]
        text=value[2]
        db_util.create_fv_user_his(user_id,fv_id,utime,text)
#この後消す
def get_low_point_user_id_by_category(category_id):
    import db_util
    row=db_util.select_low_point_user_from_user_point_by_category(category_id)
    user_id=row[0]
    return(user_id)

def eval_relation_point(user_id,act_code,target_code,category_id):
    import db_util
    point=0
    c=0
    rows=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,target_code)
    for row in rows:
        if(row[6] is not None):
            tmp=calc_relation_point(row,category_id)
            point+=tmp
            c+=1
    if(c != 0):
        point=(point*10)//c
    else:
        point=-1
    return(point)

def update_core_point3(user_id,utime):
    import db_util
    point=100
    category_id=0
    c=0
    rows=db_util.select_user_relation_point(user_id)
    for row in rows:
        if(int(row[5])!=-1):
            if(int(row[5])<point):
                point=int(row[5])
                category_id=int(row[4])
                c+=1
    if(c==0):
        point=-1
    db_util.update_core_point_3(user_id,utime,point,category_id)

def update_core_point4(user_id,utime):
    import db_util
    point=101
    category=0
    c=0
    rows=db_util.select_user_detail_by_userid(user_id)
    for category_id in range(1,CATEGORY_NUM+1):
        point_list=list()
        tpoint=0
        c=0
        for row in rows:
            if(row[6] is not None):
                #tmp=calc_relation_point(row,category_id)
                point_list.append(calc_relation_point(row,category_id))
        point_list=sorted(point_list)
        num=min(EVAL_NUM,len(point_list))
        for i in range(num):
            tpoint+=point_list[i]
            c+=1
        if(c != 0):
            tpoint=(tpoint*10)//c    
        if(point>tpoint):
            point=tpoint
            category=category_id
    if(point==101):
        point=-1

    db_util.update_core_point_3(user_id,utime,point,category)
    return(point)

def create_relation_point(user_id,utime):
    import db_util
    for act_code in range(1,7):
        for target_code in range(1,3):
            for category_id in range(1,CATEGORY_NUM+1):
                db_util.create_relation_point(user_id,utime,act_code,target_code,category_id)

def update_relation_point2(user_id,utime,act_code):
    import db_util
    for target_code in range(1,3):
        for category_id in range(1,CATEGORY_NUM+1):
            point=eval_relation_point(user_id,act_code,target_code,category_id)
            db_util.update_relation_point2(user_id,utime,act_code,target_code,category_id,point)

def create_relation_point2(user_id,utime):
    import db_util
    for act_code in range(1,7):
        for target_code in range(1,3):
            for category_id in range(1,CATEGORY_NUM+1):
                for index_num in range(1,INDEX_NUM+1):
                    db_util.create_relation_point2(user_id,utime,act_code,target_code,category_id,index_num)

"""


#============================================================
#カテゴリ総数 + 1
CATEGORY_NUM=None
#tweet確保数
INDEX_NUM=None
#評価数
EVAL_NUM=None

#手順のみを組み合わせるもの
def initial_setup(category_num,index_num,eval_num):
    global CATEGORY_NUM
    global INDEX_NUM
    global EVAL_NUM
    CATEGORY_NUM=category_num
    INDEX_NUM=index_num
    EVAL_NUM=eval_num

def update_self_points(user_id,self_info,utime,o_point):
    import db_util
    import util

    create_user_point(user_id)
    for point_id in range(1,CATEGORY_NUM+1):
        value = util.eval_text(o_point,self_info,point_id)
        point_id=value[0]
        point=value[1]
        db_util.update_self_point(user_id,point_id,utime,point)
    return()

def calc_relation_point(row,category_id):
    import util
    point=10
    text=row[6]

    value=util.eval_text(point,text,category_id)
    point=value[1]
    return(point)

def create_r_user(dictionary):
    import db_util
    import util
    for id,value in dictionary.items():
        name=value[0]
        self_info=value[1]
        follow=value[3]
        follower=value[4]
        tweets_num=value[5]
        fv_num=value[6]
        create_date=util.replace_datetime(value[8])
        db_util.create_user(name,id,self_info)
        db_util.update_user3(name,id,self_info,follow,follower,tweets_num,fv_num,create_date)

def create_user_point(user_id):
    import db_util
    for i in range(1,CATEGORY_NUM+1):
        db_util.create_user_point(user_id,i)

def create_r_user_point(dictionary):
    for r_id,value in dictionary.items():
        name=value[0]
        self_info=value[1]
        create_user_point(r_id)

def update_r_user_self_points(dictionary,utime,point):
    for r_id,value in dictionary.items():
        name=value[0]
        self_info=value[1]
        update_self_points(r_id,self_info,utime,point)

def update_user_pre_point(user_id,utime):
    import db_util
    point=100
    pre_category_id=0
    c=0
    rows=db_util.select_all_user_point(user_id)
    for row in rows:
        if(int(row[2])!=-1):
            if point>int(row[2]):
                point=int(row[2])
                pre_category_id=int(row[1])
                c+=1
    if(c==0):
        point=-1
    db_util.update_user_pre_point2(user_id,utime,point,pre_category_id)

def update_r_user_pre_point(dictionary,utime):
    for r_id,value in dictionary.items():
        update_user_pre_point(r_id,utime)

def get_low_point_user_id():
    import db_util
    row=db_util.select_low_point_user_from_user_point()
    user_id=row[0]
    return(user_id)

def get_low_point_user_id_by_category2(category_id):
    import db_util
    row=db_util.select_low_point_user_from_user_point_by_category(category_id)
    return(row)

def eval_old_user_pre_point(point):
    import time
    import db_util
    row=True
    while(row):
        utime=int(time.time())
        value=db_util.select_old_user_self_point2(utime)
        row=value[0]
        SEC=value[1]

        if(row[3] is None):
            ttime=0 
        else:
            ttime=row[3]
        
        if(ttime < (utime-SEC)):
            user_row=db_util.select_user(row[0])
            if user_row==False:
                etime=2147483647
                result=db_util.update_user_point_self_date(row[0],etime)

            else:    
                user_id=user_row[4]
                self_info=user_row[6]
                #self情報をアップデート、のちのち消してもいいかも
                create_user_detail(user_id,utime,1)
                update_user_detail_self_info_only(user_id,utime)
                
                #create_relation_point2(user_id,utime)
                for act_code in range(1,7):
                    update_relation_point3(user_id,utime,act_code)
                
                update_self_points(user_id,self_info,utime,point)
                print("update_user_pre_point:"+str(user_id))
        else:
            row=False

#カテゴリ
def create_user_detail(user_id,utime,act_code):
    import db_util
    if(act_code==1):
        rows=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,1)
        if(len(rows)==0):
            db_util.create_user_detail(user_id,act_code,1)    
    else:
        rows=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,1)
        if(len(rows)!=INDEX_NUM):
            num=INDEX_NUM-len(rows)
            for i in range(num):
                db_util.create_user_detail(user_id,act_code,1)
    
    rows=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,2)
    if not (rows):
        for i in range(INDEX_NUM):
            db_util.create_user_detail(user_id,act_code,2)   
    
    elif(len(rows)!=INDEX_NUM):
        num=INDEX_NUM-len(rows)
        for i in range(num):
            db_util.create_user_detail(user_id,act_code,2)

def update_user_detail(user_id,dictionary,utime,act_code):
    import time
    import db_util
    for fv_id,value in dictionary.items():
        name=value[0]
        self_info=value[1]
        text=value[2]
        
        #print(str(int(time.time())-utime)+":update_user_detail:start")
        #INFO
        row=db_util.select_user_detail_by_date(user_id,act_code,1)
        #print(str(int(time.time())-utime)+":update_user_detail:select2")
        id=row[0]
        db_util.update_user_detail(id,fv_id,utime,self_info)
        #print(str(int(time.time())-utime)+":update_user_detail:update3")
        
        #TEXT
        row=db_util.select_user_detail_by_date(user_id,act_code,2)
        #print(str(int(time.time())-utime)+":update_user_detail:select4")
        id=row[0]
        db_util.update_user_detail(id,fv_id,utime,text)
        #print(str(int(time.time())-utime)+":update_user_detail:update5")

def update_user_detail_self(user_id,dictionary,utime,act_code):
    import time
    import db_util

    #INFO
    value=db_util.select_user(user_id)
    self_info=value[6]
    row=db_util.select_user_detail_by_date(user_id,act_code,1)
    id=row[0]
    db_util.update_user_detail(id,user_id,utime,self_info)
    for fv_id,value in dictionary.items():
        name=value[0]
        self_info=value[1]
        text=value[2]
        #TEXT
        row=db_util.select_user_detail_by_date(user_id,act_code,2)
        #print(str(int(time.time())-utime)+":update_user_detail:select4")
        id=row[0]
        db_util.update_user_detail(id,user_id,utime,text)
        #print(str(int(time.time())-utime)+":update_user_detail:update5")

def update_user_detail_self_info_only(user_id,utime):
    import time
    import db_util

    #INFO
    value=db_util.select_user(user_id)
    self_info=value[6]
    row=db_util.select_user_detail_by_date(user_id,1,1)
    id=row[0]
    db_util.update_user_detail(id,user_id,utime,self_info)


def update_relation_point3(user_id,utime,act_code):
    import db_util
    for target_code in range(1,3):
        for category_id in range(1,CATEGORY_NUM+1):
            rows=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,target_code)
            for index_num,row in enumerate(rows):
                if(row[6] is not None):
                    point=calc_relation_point(row,category_id)
                    if point < 10:
                        db_util.update_relation_point3(user_id,utime,act_code,target_code,category_id,point,(index_num+1))
"""
def update_core_point5(user_id,utime):
    import db_util
    point=100
    category=0
    rows=db_util.select_user_relation_point_sum_by_category(user_id)
    for row in rows:
        tpoint=int(10*row[2])
        category_id=row[1]
        db_util.update_relation_point(user_id,utime,tpoint,category_id)
        if(point>tpoint):
            point=tpoint
            category=category_id

    db_util.update_core_point_3(user_id,utime,point,category)
    return(point)
"""
def update_core_point6(user_id,utime):
    import db_util
    point=100
    category=0
    for category_id in range(1,CATEGORY_NUM+1):
        cate_point=0
        count=0
        rows=db_util.select_user_relation_point_sum_by_category2(user_id,category_id)
        
        for row in rows:
            cate_point+=int(10*row[5])
            count+=1
            if count == 30:
                break
        cate_point+=max((30-len(rows))*100,0)
        cate_point=cate_point//30

        db_util.update_relation_point(user_id,utime,cate_point,category_id)
        if(point>cate_point):
            point=cate_point
            category=category_id

    db_util.update_core_point_3(user_id,utime,point,category)
    return(point)

def update_autor(user_id,autor):
    import util
    import db_util
    import time
    #ユーザーアップデート
    if len(autor)==0: 
        status=9
    else:
        name=autor[0]
        self_info=autor[1]
        follow=autor[2]
        follower=autor[3]
        tweets_num=autor[4]
        fv_num=autor[5]
        rt_num=autor[6]
        create_date=util.replace_datetime(autor[7])
        latest_date=util.replace_datetime(autor[8])
        status=0
        db_util.create_user(name,user_id,self_info)
        db_util.update_user2(name,user_id,self_info,follow,follower,tweets_num,fv_num,rt_num,create_date,latest_date)
    
    db_util.update_user_status(user_id,status)

    utime = int(time.time())
    db_util.update_relation_point_date(user_id,utime)

    if len(autor)==0:
        return(False)

    return(True) 

def update_user_text2(user_id):
    import db_util
    import sudachi_util
    import time

    w_dict={}
    utime=int(time.time())
    sudachi = sudachi_util.SudachiTokenizer()
    point=0
    for act_code in range(1,7):
        for target_code in range(1,3):
            rows=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,target_code)
            w_dict=sudachi.sudachi_text(rows,w_dict)
            w_dict=sorted(w_dict.items(), key=lambda x:x[1] ,reverse=True)
            
            for r,text in enumerate(w_dict):
                num=text[1]
                word=text[0]
                db_util.update_user_words(user_id,r+1,word,act_code,target_code,point,num,utime)

def update_user_subcategory(user_id):
    import db_util
    import time
    
    utime=int(time.time())
    row=db_util.select_user(user_id)
    point=row[1]
    category_id=row[7]
    db_util.update_user_subcategory(user_id, category_id, point,utime)

def update_subcategory_word_list_all():
    import time
    for category_id in range(1,CATEGORY_NUM+1):
        for subcategory_id in range(0,2):
            print("update_subcategory_word_list")
            print("category_id:"+str(category_id))
            print("subcategory_id:"+str(subcategory_id))
            update_subcategory_word_list(category_id,subcategory_id)
            dupcheck_subcategory_word_list(category_id,subcategory_id)
            time.sleep(1800)
    print("update_subcategory_word_list")
    print("category_id:0")
    print("subcategory_id:0")
    update_subcategory_word_list_category_0()

def update_subcategory_word_list_category_0():
    import sudachi_util
    import db_util
    sudachi = sudachi_util.SudachiTokenizer()
    
    rows=db_util.select_user_subcategory_orderbydate(0,2000)
    for act_code in range(1,7):
        for target_code in range(1,3):
            w_dict={}
            for row in rows:
                user_id=row[0]
                row2=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,target_code)
                w_dict=sudachi.sudachi_text(row2,w_dict)

            w_dict=sorted(w_dict.items(), key=lambda x:x[1] ,reverse=True)
            if len(w_dict) > 1000:
                w_dict=w_dict[:1000]
            for ranking,text in enumerate(w_dict):
                word=text[0]
                num=text[1]
                db_util.update_subcategory_word_list(0,0,target_code,act_code,ranking+1,word,num)
            

def update_subcategory_word_list(category_id,sub_category):
    import sudachi_util
    import db_util
    sudachi = sudachi_util.SudachiTokenizer()
    
    rows=db_util.select_user_subcategory_forecast(category_id,sub_category)
    for act_code in range(1,7):
        for target_code in range(1,3):
            w_dict={}
            for row in rows:
                user_id=row[0]
                row2=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,target_code)
                w_dict=sudachi.sudachi_text(row2,w_dict)

            w_dict=sorted(w_dict.items(), key=lambda x:x[1] ,reverse=True)
            if len(w_dict) > 100:
                w_dict=w_dict[:100]
            for ranking,text in enumerate(w_dict):
                word=text[0]
                num=text[1]
                db_util.update_subcategory_word_list(category_id,sub_category,target_code,act_code,ranking+1,word,num)
            

def dupcheck_subcategory_word_list(category_id,sub_category):
    import db_util
    for act_code in range(1,7):
        for target_code in range(1,3):
            rows=db_util.select_subcategory_word_list(category_id,act_code,target_code)
            for row in rows:
                word=row[5]
                rows2=db_util.select_subcategory_word_list_duplicate(0,0,word,act_code,target_code)
                if len(rows2)!=0:
                    row2=rows2[0]
                    ranking=row2[4]
                    db_util.update_subcategory_word_list_duplicate(category_id,act_code,target_code,ranking)

def update_corecore_point(user_id,category_id,subcategory_id):
    import db_util
    import util
    import time

    utime=int(time.time())
    penalty=0
    point=0

    rows2=db_util.select_user_point_by_category(user_id,category_id)
    row2=rows2[0]
    point=row2[4]

    row=db_util.select_user_detail_by_userid(user_id)
    for act_code in range(1,7):
        for target_code in range(1,3):
            rows=db_util.select_user_detail_by_userid_actcode_targetcode(user_id,act_code,target_code)
            for row in rows:
                text=row[6]
                penalty+=util.eval_subcategory_text(act_code,target_code,category_id,subcategory_id,text)
    
    sub_point=int(point+penalty)
    result=db_util.update_user_subpoint(user_id, category_id, point, subcategory_id, sub_point, utime)
    
def check_follow_user(category_id,subcategory_id):
    import tw_util
    import db_util
    follow_dictionary = tw_util.get_user_follows(1264814323668402177,0)
    for user_id in follow_dictionary:
        db_util.update_follow_list(user_id,category_id,subcategory_id,1)





