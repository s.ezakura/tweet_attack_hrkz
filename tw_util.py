INDEX_NUM=None

def retweet_user_list(name):
    import tweepy
    from config import CONFIG
    import time
    import util 

    CONSUMER_KEY = CONFIG["CONSUMER_KEY"]
    CONSUMER_SECRET = CONFIG["CONSUMER_SECRET"]
    ACCESS_TOKEN = CONFIG["ACCESS_TOKEN"]
    ACCESS_SECRET = CONFIG["ACCESS_SECRET"]

    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
    api = tweepy.API(auth)


    rt_dictionary = {}
    user_id=1
    max_id=None
    self_info=""
    for i in range(1):
        try:
            results = api.user_timeline(screen_name=name,count=200,max_id=max_id)
        except Exception as e:
            print(e)
            return(rt_dictionary,user_id,self_info)
                    
        #time.sleep(12)
        #リストがなければブレイク
        if(len(results)==0):
            break
        for result in results:
            #time.sleep(0)
            if(user_id==1):
                user_id=result.user.id
                self_info=util.replace_self_info(result.user.description)
            max_id=result.id
            #print("max_id:" + str(max_id))
            try:
                if (result.retweeted_status is not None) :
                    print(result.text + "はリツイートです")
                    print(result.retweeted_status.text + "が原文です")
                    
                    key=result.retweeted_status.user.id
                    if key not in rt_dictionary:    #1
                        rt_dictionary[key] = []     #2
                        rt_dictionary[key].append(result.retweeted_status.user.screen_name) #3
                        rt_dictionary[key].append(util.replace_self_info(result.retweeted_status.user.description)) #info
                else :
                    print("イレギュラー")
            except Exception as e:
                try:
                    print(result.text + "はリツイートではありません")
                except Exception as e:
                    print(e)
                    print("リツイートではありません")
            print()
            if(len(rt_dictionary)>=INDEX_NUM):
                break
        if(len(rt_dictionary)>=INDEX_NUM):
            break

    #print(rt_dictionary)
    return(rt_dictionary,user_id,self_info)

def retweet_user_list2(user_id):
    import time
    import util
    api=tweet_setup()
    time.sleep(15)

    rt_dictionary = {}
    max_id=None
    autor=list()
    try:
        results = api.user_timeline(id=user_id,count=200,max_id=max_id)
        for result in results:
            max_id=result.id
            try:
                if (result.retweeted_status is not None) :          
                    key=result.retweeted_status.user.id
                    if key not in rt_dictionary:    #1
                        rt_dictionary[key] = []     #2
                        rt_dictionary[key].append(result.retweeted_status.user.screen_name) #3
                        rt_dictionary[key].append(util.replace_self_info(result.retweeted_status.user.description)) #info
                        rt_dictionary[key].append(util.replace_self_info(result.retweeted_status.text)) #info
                        rt_dictionary[key].append(result.retweeted_status.user.friends_count) 
                        rt_dictionary[key].append(result.retweeted_status.user.followers_count) 
                if(len(rt_dictionary)>=INDEX_NUM):
                    break
            except Exception as e:
                #print(e)
                None
        autor.append(result.user.screen_name)
        autor.append(result.user.description)
        autor.append(result.user.friends_count)
        autor.append(result.user.followers_count)
        autor.append(result.user.statuses_count)
        autor.append(result.user.favourites_count)
        autor.append(len(rt_dictionary))
        autor.append(result.user.created_at)
        autor.append(results[0].created_at)

    except Exception as e:
        print(e)

    return(rt_dictionary,autor)

#--------------------------------------------
def initial_setup(index_num):
    global INDEX_NUM    
    INDEX_NUM=index_num

def initial_config_setup(conf_num):
    from config import CONFIG
    global CONSUMER_KEY
    global CONSUMER_SECRET
    global ACCESS_TOKEN
    global ACCESS_SECRET
    
    if conf_num == 0:
        CONSUMER_KEY = CONFIG["CONSUMER_KEY"]
        CONSUMER_SECRET = CONFIG["CONSUMER_SECRET"]
        ACCESS_TOKEN = CONFIG["ACCESS_TOKEN"]
        ACCESS_SECRET = CONFIG["ACCESS_SECRET"]

    if conf_num == 16:
        CONSUMER_KEY="mkUewnIhyq98Hwaicjo3a7ac2"
        CONSUMER_SECRET="7uhhndMSF85gBkU3bISu0pjCJdzzgTUcvl0jghL37qQiN5GByI"
        ACCESS_TOKEN="1264814323668402177-4b2LvjP0Bvu3G8YA3DC6NogOhO7BVS"
        ACCESS_SECRET="1IqazrkKIxIcnv0HPmJANq8AmQ5VUBOIil1z1O4jV311w"


def tweet_setup():
    import tweepy
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
    api = tweepy.API(auth)
    return(api)

def favorites_user_list(user_id):
    import time
    import util
    api=tweet_setup()
    time.sleep(5)
    
    fv_dictionary = {}
    try:
        results = api.favorites(id=user_id,count=20)
        for result in results:
            #print(result)
            key=result.user.id
            if key not in fv_dictionary:    #1
                fv_dictionary[key] = []     #2
                fv_dictionary[key].append(result.user.screen_name) #3
                fv_dictionary[key].append(util.replace_self_info(result.user.description)) #info
                fv_dictionary[key].append(util.replace_self_info(result.text))
                fv_dictionary[key].append(result.user.friends_count)
                fv_dictionary[key].append(result.user.followers_count)
                fv_dictionary[key].append(result.user.statuses_count)
                fv_dictionary[key].append(result.user.favourites_count)
                fv_dictionary[key].append(-1)
                fv_dictionary[key].append(result.user.created_at)
                fv_dictionary[key].append(-1)
            #print("--------------------------------------")
            
            if(len(fv_dictionary)>=INDEX_NUM):
                break
    except Exception as e:
        print(e)
    return(fv_dictionary)


def get_user_timeline(user_id):
    import time
    import util
    api=tweet_setup()
    time.sleep(5)

    self_dictionary = {}
    rt_dictionary = {}
    self_c=1
    max_id=None
    autor=list()
    try:
        results = api.user_timeline(id=user_id,count=200,max_id=max_id)
        for result in results:
            max_id=result.id
            try:
                if (result.retweeted_status is not None) :          
                    key=result.retweeted_status.user.id
                    if key not in rt_dictionary:    #1
                        rt_dictionary[key] = []     #2
                        rt_dictionary[key].append(result.retweeted_status.user.screen_name) #3
                        rt_dictionary[key].append(util.replace_self_info(result.retweeted_status.user.description)) #info
                        rt_dictionary[key].append(util.replace_self_info(result.retweeted_status.text)) #text
                        rt_dictionary[key].append(result.retweeted_status.user.friends_count) 
                        rt_dictionary[key].append(result.retweeted_status.user.followers_count) 
                        rt_dictionary[key].append(result.retweeted_status.user.statuses_count)
                        rt_dictionary[key].append(result.retweeted_status.user.favourites_count)
                        rt_dictionary[key].append(-1)
                        rt_dictionary[key].append(result.retweeted_status.user.created_at)
                        rt_dictionary[key].append(-1)
                if((len(rt_dictionary)>=INDEX_NUM) and (self_c>=INDEX_NUM)):
                    break
            except Exception as e:
                if ("@" not in util.replace_self_info(result.text)):
                    self_dictionary[self_c] = []     #2
                    self_dictionary[self_c].append(result.user.screen_name) #3
                    self_dictionary[self_c].append(util.replace_self_info(result.user.description)) #info
                    self_dictionary[self_c].append(util.replace_self_info(result.text)) #text
                    self_dictionary[self_c].append(result.user.friends_count) 
                    self_dictionary[self_c].append(result.user.followers_count) 
                    self_c+=1
                    #print(e)
                if((len(rt_dictionary)>=INDEX_NUM) and (self_c>=INDEX_NUM)):
                    break

        autor.append(result.user.screen_name)
        autor.append(result.user.description)
        autor.append(result.user.friends_count)
        autor.append(result.user.followers_count)
        autor.append(result.user.statuses_count)
        autor.append(result.user.favourites_count)
        autor.append(len(rt_dictionary))
        autor.append(result.user.created_at)
        autor.append(results[0].created_at)

    except Exception as e:
        print(e)

    return(self_dictionary,rt_dictionary,autor)

def get_user_follows(user_id,flag):
    import time
    import util
    api=tweet_setup()
    time.sleep(5)
    text=""
    max_id=None
    follow_dictionary = {}
    for i in range(100):
        try:
            results = api.friends(id=user_id,count=200,max_id=max_id)
            if(len(results)==0):
                break
            for result in results:
                max_id=result.id
                key=result.id
                if key not in follow_dictionary:    #1
                    follow_dictionary[key] = []     #2
                    follow_dictionary[key].append(result.screen_name) #3
                    follow_dictionary[key].append(util.replace_self_info(result.description)) #info
                    follow_dictionary[key].append(util.replace_self_info(text)) #text
                    follow_dictionary[key].append(result.friends_count) 
                    follow_dictionary[key].append(result.followers_count) 
                    follow_dictionary[key].append(result.statuses_count)
                    follow_dictionary[key].append(result.favourites_count)
                    follow_dictionary[key].append(-1)
                    follow_dictionary[key].append(result.created_at)
                    follow_dictionary[key].append(-1)
                    #print(result)
                if(len(follow_dictionary)>=INDEX_NUM):
                    break
        except Exception as e:
            print(e)
        if(flag==0):
            break
        
    return(follow_dictionary)

def search_keyword(word):
    import db_util
    import util
    api=tweet_setup()
    results = api.search(q=word, count=200)

    for result in results:
        user_id = result.user.id
        if db_util.select_user(user_id) == False:
            return(user_id) 





