import util
import db_util
import time

#ユーザーセット
user_name="IHayato"
utime = int(time.time())

#リツイートユーザリスト作成
rt_dictionary,user_id = util.retweet_user_list(user_name)

#ユーザー作成
db_util.create_user(user_name,user_id,utime)

#関係ユーザフラグオフ
db_util.update_hist_users(user_id)

#関係ユーザ必要数作成
db_util.create_r_user(rt_dictionary,utime)

#関係ユーザポイントセット
db_util.create_r_user_res(user_id,rt_dictionary,utime)

#ユーザコアポイントセット
db_util.update_core_point(user_id)
