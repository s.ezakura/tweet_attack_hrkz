#後で消す
def eval_self_info_id00(point,self_info):
    point_id=1
    if("稼ぐ" in self_info):
        point-=2
    if("ネットビジネス" in self_info):
        point-=2
    if("月6桁" in self_info):
        point-=2
    if("月7桁" in self_info):
        point-=2
    if("万円" in self_info):
        point-=2
    if("年収" in self_info):
        point-=2
    if("月収" in self_info):
        point-=2
    if("企業" in self_info):
        point-=1
    if("社長" in self_info):
        point-=1
    if("副業" in self_info):
        point-=1
    if("副収入" in self_info):
        point-=1
    if("発信" in self_info):
        point-=1
    if("多角" in self_info):
        point-=1
    if("せどり" in self_info):
        point-=1
    if("ノウハウ" in self_info):
        point-=1
    if(point<0):
        point=0

    return(point)

def eval_self_info(point,self_info):
    point_list=list()
    point_list.append(eval_self_info_id01(point,self_info))

    return(point_list)

#せどりクラスタ：いとう社長
def eval_self_info_id01(point,self_info):
    point_id=1
    if("稼ぐ" in self_info):
        point-=2
    if("ネットビジネス" in self_info):
        point-=2
    if("月6桁" in self_info):
        point-=2
    if("月7桁" in self_info):
        point-=2
    if("万円" in self_info):
        point-=2
    if("年収" in self_info):
        point-=2
    if("月収" in self_info):
        point-=2
    if("企業" in self_info):
        point-=1
    if("社長" in self_info):
        point-=1
    if("副業" in self_info):
        point-=1
    if("副収入" in self_info):
        point-=1
    if("発信" in self_info):
        point-=1
    if("多角" in self_info):
        point-=1
    if("せどり" in self_info):
        point-=1
    if("ノウハウ" in self_info):
        point-=1
    if(point<0):
        point=0

    return(point_id,point)
#脱サラクラスタ：イケハヤ
def eval_self_info_id02(point,self_info):
    point_id=2
    if("起業家" in self_info):
        point-=2
    if("革命" in self_info):
        point-=2
    if("やるしかない" in self_info):
        point-=2
    if("激務" in self_info):
        point-=2
    if("パワハラ" in self_info):
        point-=2
    if("セミナー" in self_info):
        point-=2
    if("在宅" in self_info):
        point-=2
    if("代表" in self_info):
        point-=2
    if("ブラック" in self_info):
        point-=2
    if("ビジネスモデル" in self_info):
        point-=1
    if("コンテンツ" in self_info):
        point-=1
    if("ジャーナリスト" in self_info):
        point-=1

    if(point<0):
        point=0

    return(point_id,point)
#ネトウヨクラスタ:百田直樹
def eval_self_info_id03(point,self_info):
    point_id=3
    if("日本第一党" in self_info):
        point-=2
    if("政権批判" in self_info):
        point-=1
    if("民主主義" in self_info):
        point-=1
    if("中国" in self_info):
        point-=1
    if("韓国" in self_info):
        point-=1
    if("安全保障" in self_info):
        point-=2
    if("日本" in self_info):
        point-=1
    if("大好き" in self_info):
        point-=1
    if("この国" in self_info):
        point-=2
    if("疑獄" in self_info):
        point-=1
    if("反日" in self_info):
        point-=2
    if("真実" in self_info):
        point-=2
    if("領土" in self_info):
        point-=2
    if("愛国" in self_info):
        point-=2
    if("国民" in self_info):
        point-=2

    if(point<0):
        point=0
    return(point_id,point)
#エンジニアクラスタ：Kazuho Oku/4005291
def eval_self_info_id04(point,self_info):
    point_id=4
    if("404" in self_info):
        point-=2
    if("技術" in self_info):
        point-=2
    if("コミッター" in self_info):
        point-=2
    if("コード" in self_info):
        point-=2
    if("コミット" in self_info):
        point-=2
    if("バイト" in self_info):
        point-=2
    if("ソース" in self_info):
        point-=2
    if("エンジニア" in self_info):
        point-=2
    if("Engineer" in self_info):
        point-=2
    if("言語" in self_info):
        point-=2
    if("分散" in self_info):
        point-=2
    if("ストレージ" in self_info):
        point-=2
    if("オブジェクト" in self_info):
        point-=2
    if("クラウド" in self_info):
        point-=2
    if("暗号" in self_info):
        point-=2
    if("Linux" in self_info):
        point-=2
    if("ネットワーク" in self_info):
        point-=2
    if("Network" in self_info):
        point-=2
    if("JavaScript" in self_info):
        point-=2
    if("ソフトウェア" in self_info):
        point-=2

    if(point<0):
        point=0
    return(point_id,point)    
#ビットコインクラスタ：hat_fx/498524336
def eval_self_info_id05(point,self_info):
    point_id=5
    if("トレーダー" in self_info):
        point-=2
    if("仮想通貨" in self_info):
        point-=3
    if("暗号資産" in self_info):
        point-=3
    if("クリプト" in self_info):
        point-=3
    if("取引所" in self_info):
        point-=2
    if("ビットコイン" in self_info):
        point-=3
    if("相場" in self_info):
        point-=2
    if("含み" in self_info):
        point-=2
    if("損" in self_info):
        point-=2
    if("得" in self_info):
        point-=2
    if("BTC" in self_info):
        point-=3
    if("投資" in self_info):
        point-=2
    if("イーサリアム" in self_info):
        point-=3
    
    if(point<0):
        point=0
    return(point_id,point)    
#ハロプロクラスタ：?/?
def eval_self_info_id06(point,self_info):
    point_id=6
    if("ハロプロ" in self_info):
        point-=4
    if("ハロオタ" in self_info):
        point-=4
    if("鈴木愛理" in self_info):
        point-=4
    if("娘オタ" in self_info):
        point-=4
    if("モー娘" in self_info):
        point-=4

    if(point<0):
        point=0
    return(point_id,point) 

def calc_core_point(rows):
    for row in rows:
        if(int(row[5])!=-1):
            point+=int(row[5])
            c+=1
    if(c!=0):
        point=point//c
    else:
        point=-1
    return()

#==================================================
CATEGORY_NUM=None
#計算を伴うもの
def initial_setup():
    import db_util
    import stadio
    import tw_util
    import front

    #再チェック期間
    SEC=2429200
    
    #tweet確保数
    INDEX_NUM=20
    #tweet確保数
    EVAL_NUM=30
    #カテゴリ総数
    global CATEGORY_NUM
    CATEGORY_NUM=16
    db_util.db_connect()
    stadio.initial_setup(CATEGORY_NUM,INDEX_NUM,EVAL_NUM)
    db_util.initial_setup(SEC)
    tw_util.initial_setup(SEC)
    front.initial_setup(CATEGORY_NUM)
    return(True)

def get_category_num():
    return(CATEGORY_NUM)

def replace_self_info(self_info):
    self_info=str(self_info).replace("\"","")
    return(self_info)

def remove_emoji(text):
    import emoji
    return ''.join(c for c in text if c not in emoji.UNICODE_EMOJI)

def replace_linebreak(self_info):
    self_info=remove_emoji(str(self_info))
    self_info=str(self_info).replace("\n","")
    self_info=str(self_info).replace("(","")
    self_info=str(self_info).replace(")","")
    self_info=str(self_info).replace("（","")
    self_info=str(self_info).replace("）","")
    self_info=str(self_info).replace("\u200d","")
    return(self_info)

def replace_datetime(datestr):
    import time
    dateint=int(datestr.timestamp())
    return(dateint)

def eval_text(point,text,point_id):
    #せどりクラスタ：いとう社長
    if(point_id==1):    
        if("稼ぐ" in text):
            point-=2
        if("ネットビジネス" in text):
            point-=2
        if("月6桁" in text):
            point-=1
        if("月7桁" in text):
            point-=1
        if("万円" in text):
            point-=1
        if("年収" in text):
            point-=1
        if("月収" in text):
            point-=2
        if("企業" in text):
            point-=1
        if("副業" in text):
            point-=2
        if("副収入" in text):
            point-=1
        if("発信" in text):
            point-=1
        if("多角" in text):
            point-=1
        if("せどり" in text):
            point-=2
        if("ノウハウ" in text):
            point-=2
        if("達成" in text):
            point-=2
        if("開始" in text):
            point-=2
        if("人生" in text):
            point-=2
        if(point<0):

            point=0
        return(point_id,point)

    #脱サラクラスタ：イケハヤ
    if(point_id==2):
        if("起業家" in text):
            point-=2
        if("革命" in text):
            point-=2
        if("やるしかない" in text):
            point-=2
        if("激務" in text):
            point-=2
        if("パワハラ" in text):
            point-=2
        if("セミナー" in text):
            point-=2
        if("在宅" in text):
            point-=2
        if("代表取締役" in text):
            point-=2
        if("ブラック" in text):
            point-=2
        if("ビジネスモデル" in text):
            point-=1
        if("コンテンツ" in text):
            point-=1
        if("ジャーナリスト" in text):
            point-=1
        
        if("公式アカ" in text):
            point+=5
        if("大革命" in text):
            point+=5
        
        if(point<0):
            point=0
        if(point>10):
            point=10
        return(point_id,point)

    #ネトウヨクラスタ:百田直樹
    if(point_id==3):    
        if("日本第一党" in text):
            point-=2
        if("政権批判" in text):
            point-=1
        if("民主主義" in text):
            point-=1
        if("中国" in text):
            point-=2
        if("韓国" in text):
            point-=2
        if("安全保障" in text):
            point-=2
        if("日本" in text):
            point-=1
        if("大好き" in text):
            point-=1
        if("この国" in text):
            point-=2
        if("疑獄" in text):
            point-=1
        if("反日" in text):
            point-=2
        if("真実" in text):
            point-=2
        if("領土" in text):
            point-=2
        if("愛国" in text):
            point-=2
        if("国民" in text):
            point-=2
        if("守る" in text):
            point-=2
        if(point<0):
            point=0
        return(point_id,point)

    #エンジニアクラスタ：Kazuho Oku/4005291
    if(point_id==4):    
        if("404" in text):
            point-=2
        if("技術" in text):
            point-=2
        if("コミッター" in text):
            point-=2
        if("コード" in text):
            point-=2
        if("コミット" in text):
            point-=2
        if("バイト" in text):
            point-=2
        if("ソース" in text):
            point-=2
        if("エンジニア" in text):
            point-=2
        if("Engineer" in text):
            point-=2
        if("言語" in text):
            point-=2
        if("分散" in text):
            point-=2
        if("ストレージ" in text):
            point-=2
        if("オブジェクト" in text):
            point-=2
        if("クラウド" in text):
            point-=2
        if("暗号" in text):
            point-=2
        if("Linux" in text):
            point-=2
        if("ネットワーク" in text):
            point-=2
        if("Network" in text):
            point-=2
        if("JavaScript" in text):
            point-=2
        if("ソフトウェア" in text):
            point-=2
        if("AWS" in text):
            point-=2
        if("Linux" in text):
            point-=2
        if("ruby" in text):
            point-=2
        if("インフラ" in text):
            point-=2
        if(point<0):
            point=0
        return(point_id,point)    

    #ビットコインクラスタ：hat_fx/498524336
    if(point_id==5):
        if("トレーダー" in text):
            point-=1
        if("仮想通貨" in text):
            point-=3
        if("暗号資産" in text):
            point-=3
        if("暗号通貨" in text):
            point-=3
        if("クリプト" in text):
            point-=3
        if("取引所" in text):
            point-=2
        if("ビットコイン" in text):
            point-=3
        if("相場" in text):
            point-=2
        if("含み" in text):
            point-=2
        if("BTC" in text):
            point-=3
        if("投資" in text):
            point-=1
        if("イーサリアム" in text):
            point-=3
        if("XRP" in text):
            point-=3
        if("リップル" in text):
            point-=3
        if("eth" in text):
            point-=3
        if(point<0):
            point=0
        return(point_id,point)    

    #ハロプロクラスタ：?/?
    if(point_id==6):    
        if("ハロプロ" in text):
            point-=4
        if("ハロオタ" in text):
            point-=4
        if("鈴木愛理" in text):
            point-=4
        if("娘オタ" in text):
            point-=4
        if("モー娘" in text):
            point-=4
        if("アンジュルム" in text):
            point-=4
        if("JUICE" in text):
            point-=4       
        if("℃" in text):
            point-=4
        if("ハロヲタ" in text):
            point-=4
        if("こぶしファクトリー" in text):
            point-=4
        if("佐藤優樹" in text):
            point-=4     
        if(point<0):
            point=0
        return(point_id,point)
    
    #十二国記クラスタ：?/?
    if(point_id==7):    
        if("十二国記" in text):
            point-=3
        if("月の影" in text):
            point-=3
        if("影の海" in text):
            point-=3
        if("風の海" in text):
            point-=3
        if("迷宮の岸" in text):
            point-=3
        if("風の万里" in text):
            point-=3
        if("黎明の空" in text):
            point-=3
        if("魔性の子" in text):
            point-=3
        if("黄昏の岸" in text):
            point-=3
        if("暁の天" in text):
            point-=3
        if("魔性の子" in text):
            point-=3
        if("華胥の幽夢" in text):
            point-=3
        if("白銀の墟" in text):
            point-=3
        if("玄の月" in text):
            point-=3
        if("陽子" in text):
            point-=2
        if("景麒" in text):
            point-=2
        if("泰麒" in text):
            point-=2
        if("戴" in text):
            point-=2
        if("主従" in text):
            point-=2     

        if("南野陽子" in text):
            point+=10

        if(point<0):
            point=0
        return(point_id,point)
    
    #関ジャニ
    if(point_id==8):
        if("関ジャニ" in text):
            point-=2
        if("村上" in text):
            point-=2
        if("丸山" in text):
            point-=2
        if("大倉" in text):
            point-=2
        if("安田" in text):
            point-=2
        if("横山" in text):
            point-=2
        if("すばる" in text):
            point-=2
        if("∞" in text):
            point-=2
        if("eighter" in text):
            point-=3
        if(point<0):
            point=0
        return(point_id,point)
    
    #競技プログラミング
    if(point_id==9):
        if("競技" in text):
            point-=2
        if("プログラミング" in text):
            point-=2
        if("競プロ" in text):
            point-=3
        if("コーダー" in text):
            point-=2
        if("AtCoder" in text):
            point-=3
        if("Codeforce" in text):
            point-=3
        if("highest" in text):
            point-=2
        if("past" in text):
            point-=2                        
        if(point<0):
            point=0
        return(point_id,point)

    #コンサルタント
    if(point_id==10):
        if("コンサル" in text):
            point-=2
        if("シニアコン" in text):
            point-=2
        if("戦略" in text):
            point-=2
        if("MBA" in text):
            point-=2
        if("MBB" in text):
            point-=2
        if("外資" in text):
            point-=2
        if(point<0):
            point=0
        return(point_id,point)

    
    #攻殻機動隊
    if(point_id==11):
        if("攻殻" in text):
            point-=3
        if("タチコマ" in text):
            point-=3
        if("バトー" in text):
            point-=3
        if("神山監督" in text):
            point-=3
        if("SAC" in text):
            point-=3
        if("笑い男" in text):
            point-=3
        if("2045" in text):
            point-=3              
        if(point<0):
            point=0
        return(point_id,point)
    
    #医師：ampc20160430/1097717755271233539
    if(point_id==12):
        if("女医" in text):
            point-=3
        if("医師" in text):
            point-=3
        if("医学" in text):
            point-=3
        if("白衣" in text):
            point-=3
        if("臨床" in text):
            point-=3    
        if("医療" in text):
            point-=3
        if("衛生" in text):
            point-=2
        if("専門医" in text):
            point-=3
        if("指導医" in text):
            point-=3
        if("緩和ケア" in text):
            point-=3
        if("感染症" in text):
            point-=1
        if("公衆衛生" in text):
            point-=2
        if("病院" in text):
            point-=1
        if("歯科" in text):
            point-=2                       
        if(point<0):
            point=0
        return(point_id,point)

    #美容：poisoncookie00/739070891909255168
    if(point_id==13):
        if("美容" in text):
            point-=3
        if("美少女" in text):
            point-=2
        if("かわいい" in text):
            point-=2
        if("コンシーラー" in text):
            point-=3
        if("メイク" in text):
            point-=3    
        if("パウダー" in text):
            point-=3
        if("コスメ" in text):
            point-=3
        if("ブルべ" in text):
            point-=3
        if("イエベ" in text):
            point-=3
        if("色白" in text):
            point-=3
        if("鼻筋" in text):
            point-=3
        if("涙袋" in text):
            point-=3
        if("プチプラ" in text):
            point-=3
        if("ブス" in text):
            point-=3
        if("ネイル" in text):
            point-=2
        if("化粧" in text):
            point-=3 
        if("プチプラ" in text):
            point-=3
        if("デパコス" in text):
            point-=3  
        if("ダイエット" in text):
            point-=2
        if("スキンケア" in text):
            point-=2
        if("骨格" in text):
            point-=2                                            
        if(point<0):
            point=0
        return(point_id,point)

    #イラスト：ERIMO_WKS/230706108
    if(point_id==14):
        if("イラスト" in text):
            point-=3
        if("新刊" in text):
            point-=2
        if("既刊" in text):
            point-=2
        if("イラストレータ" in text):
            point-=2
        if("作画" in text):
            point-=2    
        if("コミティア" in text):
            point-=2
        if("表紙絵" in text):
            point-=3
        if("絵師" in text):
            point-=3 
        if("クリエータ" in text):
            point-=3
        if("著作権" in text):
            point-=1 
        if("使用権" in text):
            point-=1                                             
        if(point<0):
            point=0
        return(point_id,point)

    #経営者：
    if(point_id==15):
        if("社長" in text):
            point-=2
        if("代表" in text):
            point-=2
        if("取締役" in text):
            point-=2
        if("創業者" in text):
            point-=2
        if("起業" in text):
            point-=2    
        if("投資" in text):
            point-=2
        if("エンジェル" in text):
            point-=3
        if("M&A" in text):
            point-=3
        return(point_id,point)
    #コスプレ:
    if(point_id==16):
        if("コスプレ" in text):
            point-=3
        if("写真" in text):
            point-=2
        if(("撮影" in text) or ("撮影会" in text)):
            point-=2
        if("衣装" in text):
            point-=2
        if("モデル" in text):
            point-=1    
        if("ポトレ" in text):
            point-=2
        if("レイヤー" in text):
            point-=2
        return(point_id,point) 

    return(point_id,point)    


def get_keyword(point_id):
    if point_id==1:
        return ("副業")
    if point_id==2:
        return ("独立")
    if point_id==3:
        return ("エンジニア")
    if point_id==4:
        return ("愛国")
    if point_id==5:
        return ("ビットコイン")
    if point_id==6:
        return ("ハロプロ")
    if point_id==7:
        return ("十二国記")
    if point_id==8:
        return ("関ジャニ")
    if point_id==9:
        return ("競プロ")
    if point_id==10:
        return ("コンサル")
    if point_id==11:
        return ("攻殻")
    if point_id==12:
        return ("医師")
    if point_id==13:
        return ("美容")
    if point_id==14:
        return ("イラスト")
    if point_id==15:
        return ("経営者")
    if point_id==16:
        return ("コスプレ")



def eval_subcategory_text(act_code,target_code,category_id,subcategory_id,text):
    point=0
    if(text is None):
        return point

    if(category_id==16):
        if(subcategory_id==1):
            if(act_code==1 and target_code==1):
                if("カメラ" in text):
                    point+=50
                if("撮って" in text):
                    point+=50
                if("撮影して" in text):
                    point+=50
                if("コスプレ撮影" in text):
                    point+=50
                if("AVサークル" in text):
                    point+=50
                if("dokin" in text):
                    point+=50
                if("フォトグラファー" in text):
                    point+=50
                if("開催" in text):
                    point+=50
    return(point)
