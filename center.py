#関数のみで構成するもの

def eval_old_user_pre_point(point):
    import stadio    
    stadio.eval_old_user_pre_point(point)

def get_timeline(user_id):
    import tw_util
    self_dictionary,re_dictionary,autor = tw_util.get_user_timeline(user_id)
    return(self_dictionary,re_dictionary,autor)

def get_re_dictionary(user_id):
    import tw_util
    re_dictionary,autor = tw_util.retweet_user_list2(user_id)
    return(re_dictionary,autor)

def get_fv_dictionary(user_id):
    import tw_util
    fv_dictionary = tw_util.favorites_user_list(user_id)
    return(fv_dictionary)

def get_follow_dictionary(user_id):
    import tw_util
    follow_dictionary = tw_util.get_user_follows(user_id,0)
    return(follow_dictionary)

def eval_user2(user_id,act_code,dictionary):
    import db_util
    import stadio
    import time

    point=10
    utime = int(time.time())

    #関係ユーザ必要数作成
    #print(str(int(time.time())-utime)+":start")
    stadio.create_r_user(dictionary)
    #print(str(int(time.time())-utime)+":create_r_user")
    stadio.create_r_user_point(dictionary)
    #print(str(int(time.time())-utime)+":create_r_user_point")
    stadio.update_r_user_self_points(dictionary,utime,point)
    #print(str(int(time.time())-utime)+":update_r_user_self_points")
    stadio.update_r_user_pre_point(dictionary,utime)
    #print(str(int(time.time())-utime)+":update_r_user_pre_point")

    #詳細作成
    #ユーザUPDATE
    stadio.create_user_detail(user_id,utime,act_code)#re=2,fv=3
    #print(str(int(time.time())-utime)+":create_user_detail")
    stadio.update_user_detail(user_id,dictionary,utime,act_code)#re=2,fv=3
    #print(str(int(time.time())-utime)+":update_user_detail")

    #リレイションポイントセット
    stadio.update_relation_point3(user_id,utime,act_code)
    #print(str(int(time.time())-utime)+":update_relation_point2")
    
    #コアポイント・ユーザポイントセット
    stadio.update_core_point6(user_id,utime)
    #print(str(int(time.time())-utime)+":update_core_point5")
    print(str(int(time.time())-utime)+":end")    

def eval_user_self(user_id,act_code,dictionary):
    import db_util
    import stadio
    import time

    utime = int(time.time())

    #詳細作成
    #ユーザUPDATE
    stadio.update_user_pre_point(user_id,utime)
    #print(str(int(time.time())-utime)+":update_user_pre_point")
    stadio.create_user_detail(user_id,utime,act_code)#self=1,#re=2,fv=3
    #print(str(int(time.time())-utime)+":create_user_detail")
    stadio.update_user_detail_self(user_id,dictionary,utime,act_code)#self=1,#re=2,fv=3
    #print(str(int(time.time())-utime)+":update_user_detail")

    #リレイションポイントセット
    stadio.update_relation_point3(user_id,utime,act_code)
    #print(str(int(time.time())-utime)+":update_relation_point2")
    
    #コアポイント・ユーザポイントセット
    stadio.update_core_point6(user_id,utime)
    #print(str(int(time.time())-utime)+":update_core_point5")
    print(str(int(time.time())-utime)+":end")    

def eval_user_all(user_id):
    import center
    import stadio
    import db_util
    import util
    import time
    print("eval user_id:"+str(user_id))

    #タイムライン
    self_dictionary,re_dictionary,autor=center.get_timeline(user_id)
    #ユーザーアップデート
    stadio.create_user_point(user_id)
    flag=stadio.update_autor(user_id,autor)
    
    if flag:
        print("no self") if len(self_dictionary)==0 else None
        center.eval_user_self(user_id,1,self_dictionary)    

        #リツイート
        print("no retweet") if len(re_dictionary)==0 else None
        center.eval_user2(user_id,2,re_dictionary)

        #いいね
        fv_dictionary=center.get_fv_dictionary(user_id)
        print("no fv") if len(fv_dictionary)==0 else None
        center.eval_user2(user_id,3,fv_dictionary)

        #フォロー
        follow_dictionary=center.get_follow_dictionary(user_id)
        print("no follow") if len(follow_dictionary)==0 else None
        center.eval_user2(user_id,5,follow_dictionary)

        utime = int(time.time())
        db_util.update_relation_point_date(user_id,utime)

def update_user_text2(user_id):
    import stadio
    stadio.update_user_text2(user_id)

def update_user_subcategory(user_id):
    import stadio
    stadio.update_user_subcategory(user_id)

def update_subcategory_word_list():
    import stadio
    stadio.update_subcategory_word_list_all()

def update_corecore_point():
    import stadio
    import db_util
    import time

    utime=int(time.time())

    for s_c_id in range(0,2) :    
        rows=db_util.select_user_subcategory_orderbydate(s_c_id,1)
        row=rows[0]
        user_id=row[0]
        category_id=row[1]
        point=row[2]
        if category_id==16:
            print("update_corecore_point user_id : "+str(user_id))
        #category_id回したほうがええ？
        for sub_category in range(0,2) :    
            db_util.update_user_subcategory(user_id, category_id, point, utime)
            stadio.update_corecore_point(user_id,category_id,sub_category)

    
